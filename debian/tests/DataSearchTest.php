<?php
declare(strict_types = 1);
namespace Williamdes\MariaDBMySQLKBS\Test;

use \PHPUnit\Framework\TestCase;
use \Williamdes\MariaDBMySQLKBS\Search;

class DataSearchTest extends TestCase
{

    /**
     * test the real data
     *
     * @return void
     */
    public function testRealData(): void
    {
        // Autopkg tests, data dir was patched by debian
        if (! is_dir(Search::$DATA_DIR)) {
            // Set back to dist folder
            Search::$DATA_DIR = __DIR__ . Search::DS . ".." . Search::DS . ".." . Search::DS . "dist" . Search::DS;
        }
        $found = Search::getByName("character_set_client", Search::MYSQL);
        $this->assertStringContainsString("https://dev.mysql.com/doc/refman/", $found);
        $this->assertStringContainsString("/en/server-system-variables.html#sysvar_character_set_client", $found);
    }

}
